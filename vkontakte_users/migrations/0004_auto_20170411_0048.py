# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import vkontakte_api.fields


class Migration(migrations.Migration):

    dependencies = [
        ('vkontakte_users', '0003_auto_20160215_1510'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='graduation',
            field=models.PositiveIntegerField(null=True, verbose_name='\u0414\u0430\u0442\u0430 \u043e\u043a\u043e\u043d\u0447\u0430\u043d\u0438\u044f \u0412\u0423\u0417\u0430'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='user',
            name='schools',
            field=vkontakte_api.fields.JSONField(default=b'{}', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='user',
            name='universities',
            field=vkontakte_api.fields.JSONField(default=b'{}', null=True, blank=True),
            preserve_default=True,
        ),
    ]
